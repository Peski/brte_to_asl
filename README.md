# brte_to_asl
Extract timestamp-image association into ASL csv fromat from BRTE format

**Author:** [David Zuñiga-Noël](http://mapir.isa.uma.es/mapirwebsite/index.php/people/270)

**License:**  [GPLv3](LICENSE.txt)

## 1. Dependencies (tested)

* CMake (3.5.1-1ubuntu1)
   ```
   sudo apt install cmake
   ```
* Boost (1.58.0.1ubuntu1)
   ```
   sudo apt install libboost-all-dev
   ```
* Gflags (2.1.2-3)
   ```
   sudo apt install libgflags-dev
   ```
   
## 2. Build

Once all dependencies are installed, proceed to build the source code with the following commands:
```
git clone https://bitbucket.org/Peski/brte_to_asl.git
mkdir build
cd build
cmake ..
make -j8
```

The compiled executable should be inside the `build` directory.

## 3. Data Format

The input text file (in BRTE format) should be a white space separated numeric only table. Rows containing non-numeric values (e.g. headers) should be preceded by the comment symbol '#'. The first column should represend a unique identifier and the second one the timestamp in seconds, with data 9 columns in total.

The output CSV file (in ASL format) has the format specified by:
```
timestamp,filename
```
with timestamp in nanoseconds.

## 4. Usage

The `brte_to_asl` tool can invoked as follows:
```
brte_to_asl [options] <images_directory> <groundtruth_file>
```
where the available options are:

* `--output_file=desired_file`: to set the output association file

