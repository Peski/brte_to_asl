// Copyright (c) 2018, ETH Zurich and UNC Chapel Hill.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//
//     * Neither the name of ETH Zurich and UNC Chapel Hill nor the names of
//       its contributors may be used to endorse or promote products derived
//       from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Author: Johannes L. Schoenberger (jsch-at-demuc-dot-de)

#ifndef GPS_HPP_
#define GPS_HPP_

#include <cmath>
#include <limits>
#include <stdexcept>
#include <vector>

#include <Eigen/Core>

#include "util/alignment.h"

// Transform ellipsoidal GPS coordinates to Cartesian GPS coordinate
// representation and vice versa.
class GPSTransform {
 public:
  enum ELLIPSOID { GRS80, WGS84 };

  explicit GPSTransform(const int ellipsoid = GRS80) {
    switch (ellipsoid) {
      case GRS80:
        a_ = 6378137;
        b_ = 6.356752314140356e+06;
        f_ = 0.003352810681182;
        break;
      case WGS84:
        a_ = 6378137;
        b_ = 6.356752314245179e+06;
        f_ = 0.003352810664747;
        break;
      default:
        a_ = std::numeric_limits<double>::quiet_NaN();
        b_ = std::numeric_limits<double>::quiet_NaN();
        f_ = std::numeric_limits<double>::quiet_NaN();
        throw std::invalid_argument("Ellipsoid not defined");
    }

    e2_ = (a_ * a_ - b_ * b_) / (a_ * a_);
  }

  Eigen::Vector3d EllToXYZ(
      const Eigen::Vector3d& ell) const {
    Eigen::Vector3d xyz;

    const double lat = ell(0); // in radians!
    const double lon = ell(1); // in radians!
    const double alt = ell(2);

    const double sin_lat = std::sin(lat);
    const double sin_lon = std::sin(lon);
    const double cos_lat = std::cos(lat);
    const double cos_lon = std::cos(lon);

    // Normalized radius
    const double N = a_ / std::sqrt(1 - e2_ * sin_lat * sin_lat);

    xyz(0) = (N + alt) * cos_lat * cos_lon;
    xyz(1) = (N + alt) * cos_lat * sin_lon;
    xyz(2) = (N * (1 - e2_) + alt) * sin_lat;

    return xyz;
  }

  std::vector<Eigen::Vector3d> EllToXYZ(
      const std::vector<Eigen::Vector3d>& ell) const {
    std::vector<Eigen::Vector3d> xyz(ell.size());

    for (size_t i = 0; i < ell.size(); ++i)
      xyz[i] = EllToXYZ(ell[i]);

    return xyz;
  }

  std::vector<Eigen::Vector3d> XYZToEll(
      const std::vector<Eigen::Vector3d>& xyz) const {
    std::vector<Eigen::Vector3d> ell(xyz.size());

    for (size_t i = 0; i < ell.size(); ++i) {
      const double x = xyz[i](0);
      const double y = xyz[i](1);
      const double z = xyz[i](2);

      const double xx = x * x;
      const double yy = y * y;

      const double kEps = 1e-12;

      // Latitude
      double lat = std::atan2(z, std::sqrt(xx + yy));
      double alt;

      for (size_t j = 0; j < 100; ++j) {
        const double sin_lat0 = std::sin(lat);
        const double N = a_ / std::sqrt(1 - e2_ * sin_lat0 * sin_lat0);
        alt = std::sqrt(xx + yy) / std::cos(lat) - N;
        const double prev_lat = lat;
        lat = std::atan((z / std::sqrt(xx + yy)) * 1 / (1 - e2_ * N / (N + alt)));

        if (std::abs(prev_lat - lat) < kEps) {
          break;
        }
      }

      ell[i](0) = lat;

      // Longitude
      ell[i](1) = std::atan2(y, x);
      // Alt
      ell[i](2) = alt;
  }

  return ell;
}

 private:
  // Semimajor axis.
  double a_;
  // Semiminor axis.
  double b_;
  // Flattening.
  double f_;
  // Numerical eccentricity.
  double e2_;
};

#endif  // GPS_HPP_
