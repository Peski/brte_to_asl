#define PROGRAM_NAME \
    "asl_to_tum"

#define FLAGS_CASES                                                                                \
    FLAG_CASE(double, time_offset, 0.0, "Timestamp offset [s]")

#define ARGS_CASES                                                                                 \
    ARG_CASE(input_file)                                                                           \
    ARG_CASE(output_file)

#include <algorithm>
#include <iostream>
#include <vector>

// Args (Gflags extension)
#include "args.hpp"

// Boost
#include <boost/filesystem/operations.hpp>
#include <boost/range/iterator_range.hpp>

// Eigen
#include <Eigen/Core>

#include "util/macros.h"
#include "util/version.h"

#include "csv.hpp"
#include "io.hpp"

namespace fs = boost::filesystem;

using imageid_t = std::size_t;

inline void ValidateFlags() { }

inline void ValidateArgs() {
    RUNTIME_ASSERT(fs::is_regular_file(ARGS_input_file));
    RUNTIME_ASSERT(!fs::exists(ARGS_output_file));
}

int main(int argc, char* argv[]) {

    // Print build info
    std::cout << PROGRAM_NAME << " (" << GetBuildInfo() << ")" << std::endl;
    std::cout << std::endl;

    // Handle help flag
    if (args::HelpRequired(argc, argv)) {
        args::ShowHelp();
        return 0;
    }

    // Parse input flags
    args::ParseCommandLineNonHelpFlags(&argc, &argv, true);

    // Check number of args
    if (argc-1 != args::NumArgs()) {
        args::ShowHelp();
        return -1;
    }

    // Parse input args
    args::ParseCommandLineArgs(argc, argv);

    // Validate input arguments
    ValidateFlags();
    ValidateArgs();

    Eigen::MatrixXd data = csv::read<double>(ARGS_input_file, ',');
    RUNTIME_ASSERT(data.cols() == 8); // 8 columns expected
    RUNTIME_ASSERT(data.rows() > 0); // invalid file?

    std::vector<io::trajectory_t<double>> output_trajectory;
    for (int idx = 0; idx < data.rows(); ++idx) {
        const Eigen::Matrix<double, 1, 8>& row_data = data.row(idx);
        double sec = row_data(0) * 1e-9 + FLAGS_time_offset; // nanosec to sec
        //WARNING: we use a trick to output qx qy qz qw format! (pose.qw represents qx, but doesn't matter since we are not converting it to Eigen::Isometry)
        io::pose_t pose(row_data(1), row_data(2), row_data(3), row_data(5), row_data(6), row_data(7), row_data(4));
        output_trajectory.emplace_back(io::trajectory_t<double>(sec, pose));
    }

    io::OUTPUT_SEPARATOR = " ";
    std::sort(output_trajectory.begin(), output_trajectory.end());
    io::write_file(output_trajectory, ARGS_output_file,
                   "#timestamp [s] tx [m] ty [m] tz [m] qx qy qz qw");

    return 0;
}

