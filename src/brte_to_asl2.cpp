#define PROGRAM_NAME \
    "brte_to_asl2"

#define FLAGS_CASES                                                                                \
    FLAG_CASE(string, images_file, "images.txt", "Input images file name")                         \
    FLAG_CASE(string, imu_file, "images_sens.txt", "Input IMU file name")                          \
    FLAG_CASE(string, gt_file, "images_truth.txt", "Input groundtruth file name")                  \
    FLAG_CASE(bool, set_origin_now, true, "Set the time origin as the now")

#define ARGS_CASES                                                                                 \
    ARG_CASE(images_directory)                                                                     \
    ARG_CASE(output_path)

#include <algorithm>
#include <cstdint>
#include <ctime>
#include <iostream>
#include <limits>
#include <unordered_map>
#include <utility>

// Args (Gflags extension)
#include "args.hpp"

// Boost
#include <boost/filesystem/operations.hpp>
#include <boost/range/iterator_range.hpp>

// Eigen
#include <Eigen/Core>

#include "util/macros.h"
#include "util/version.h"

#include "csv.hpp"
#include "gps.hpp"
#include "io.hpp"
#include "sequence.hpp"

namespace fs = boost::filesystem;

using imageid_t = std::size_t;

inline void ValidateArgs() {
    RUNTIME_ASSERT(fs::is_directory(ARGS_images_directory));

    fs::create_directories(ARGS_output_path);
    fs::path output_base = fs::path(ARGS_output_path);

    fs::create_directory(output_base / "imu0");
    RUNTIME_ASSERT(fs::is_empty(output_base / "imu0"));

    fs::create_directory(output_base / "cam0");
    RUNTIME_ASSERT(fs::is_empty(output_base / "cam0"));

    fs::create_directory(output_base / "cam0/data");
    RUNTIME_ASSERT(fs::is_empty(output_base / "cam0/data"));
}

inline void ValidateFlags() {
    fs::path input_base = fs::path(ARGS_images_directory).parent_path();
    RUNTIME_ASSERT(fs::is_regular_file(input_base / FLAGS_images_file));
    RUNTIME_ASSERT(fs::is_regular_file(input_base / FLAGS_imu_file));
    RUNTIME_ASSERT(fs::is_regular_file(input_base / FLAGS_gt_file));
}

int main(int argc, char* argv[]) {

    // Print build info
    std::cout << PROGRAM_NAME << " (" << GetBuildInfo() << ")" << std::endl;
    std::cout << std::endl;

    // Handle help flag
    if (args::HelpRequired(argc, argv)) {
        args::ShowHelp();
        return 0;
    }

    // Parse input flags
    args::ParseCommandLineNonHelpFlags(&argc, &argv, true);

    // Check number of args
    if (argc-1 != args::NumArgs()) {
        args::ShowHelp();
        return -1;
    }

    // Parse input args
    args::ParseCommandLineArgs(argc, argv);

    // Validate input arguments
    ValidateArgs();
    ValidateFlags();

    // Compute current time (if needed)
    io::timestamp_t new_origin = 0;
    if (FLAGS_set_origin_now)
        new_origin = static_cast<double>(std::time(nullptr)) * 1e9;

    // Set input and output paths
    fs::path input_base = fs::path(ARGS_images_directory).parent_path();
    fs::path output_base = fs::path(ARGS_output_path);

    // Read IMU data
    std::cout << "Reading " << FLAGS_imu_file << " ..." << std::endl;
    Eigen::MatrixXd imu_data = csv::read<double>((input_base / FLAGS_imu_file).string(), ' ');
    RUNTIME_ASSERT(imu_data.cols() == 7); // 7 columns expected
    RUNTIME_ASSERT(imu_data.rows() > 0); // invalid file?

    // Read images data
    std::cout << "Reading " << FLAGS_images_file << " ..." << std::endl;
    Eigen::MatrixXd images_data = csv::read<double>((input_base / FLAGS_images_file).string(), ' ');
    RUNTIME_ASSERT(images_data.cols() == 9); // 9 columns expected
    RUNTIME_ASSERT(images_data.rows() > 0); // invalid file?

    // Process timestamps
    std::cout << "Processing timestamps..." << std::endl;
    io::timestamp_t origin = std::numeric_limits<io::timestamp_t>::max();
    for (int i = 0; i < imu_data.rows(); ++i) {
        io::timestamp_t ns = static_cast<io::timestamp_t>(imu_data(i, 0) * 1e9);
        origin = std::min(origin, ns);
    }

    std::unordered_map<imageid_t, io::timestamp_t> id_to_timestamp;
    for (int i = 0; i < images_data.rows(); ++i) {
        imageid_t id = static_cast<imageid_t>(images_data(i, 0));
        io::timestamp_t ns =  static_cast<io::timestamp_t>(images_data(i, 1) * 1e9);

        RUNTIME_ASSERT(id_to_timestamp.find(id) == id_to_timestamp.end()); // id expected to be an unique identifier
        id_to_timestamp[id] = ns;
        origin = std::min(origin, ns);
    }

    // Process IMU
    std::cout << "Processing IMU data ..." << std::endl;
    io::ImuData imu;
    for (int i = 0; i < imu_data.rows(); ++i) {
        std::cout << '\r' << i+1 << " / " << imu_data.rows() << std::flush;
        io::timestamp_t ns = static_cast<io::timestamp_t>(imu_data(i, 0) * 1e9);
        if (FLAGS_set_origin_now) ns = (ns - origin) + new_origin;

        imu.emplace_back(io::imu_data_t{ns,
                                        imu_data(i, 4), imu_data(i, 5), imu_data(i, 6),
                                        imu_data(i, 1), imu_data(i, 2), imu_data(i, 3)});
    }
    std::cout << std::endl;

    std::sort(imu.begin(), imu.end());
    std::cout << "Saving imu0/data.csv ..." << std::flush;
    io::write_file(imu, (output_base / "imu0/data.csv").string(),
                   "#timestamp [ns],w_x [rad s^-1],w_y [rad s^-1],w_z [rad s^-1],a_x [m s^-2],a_y [m s^-2],a_z [m s^-2]");
    std::cout << std::endl;

    // Process Images
    std::cout << "Processing Images ..." << std::endl;
    std::size_t k = 0;
    io::Records records;
    //for (const fs::directory_entry& entry : boost::make_iterator_range(fs::directory_iterator(ARGS_images_directory), {})) {
        //std::string filename = entry.path().filename().string();
    std::vector<std::string> seq = getSequence(ARGS_images_directory);
    for (const std::string& file : seq) {
        std::cout << '\r' << k+1 << " / " << seq.size() << std::flush;
        fs::path entry = fs::path(file);
        std::string filename = entry.filename().string();
        imageid_t id = static_cast<imageid_t>(sequence_id::get_sid(filename));
        RUNTIME_ASSERT(id_to_timestamp.find(id) != id_to_timestamp.end()); // id expected to be found in groundtruth file

        io::timestamp_t ns = id_to_timestamp.at(id);
        if (FLAGS_set_origin_now) ns = (ns - origin) + new_origin;

        std::string new_filename = io::to_string(ns, 10) + fs::extension(filename);
        fs::path new_path = output_base / "cam0/data";
        new_path /= new_filename;

        fs::copy(entry, new_path);

        records.emplace_back(std::make_pair(ns, new_filename));
        k++;
    }
    std::cout << std::endl;

    std::sort(records.begin(), records.end());
    std::cout << "Saving cam0/data.csv ..." << std::flush;
    io::write_file(records, (output_base / "cam0/data.csv").string(),
                   "#timestamp [ns],filename");
    std::cout << std::endl;

    // Read Groundtruth data
    std::cout << "Reading " << FLAGS_gt_file << " ..." << std::endl;
    Eigen::MatrixXd data = csv::read<double>((input_base / FLAGS_gt_file).string(), ' ');
    RUNTIME_ASSERT(data.cols() == 8); // 8 columns expected
    RUNTIME_ASSERT(data.rows() > 0); // invalid file?

    GPSTransform wgs84(GPSTransform::WGS84);

    // Process Groundtruth
    std::cout << "Processing Groundtruth data ..." << std::endl;
    io::Trajectory trajectory;
    for (int i = 0; i < data.rows(); ++i) {
        std::cout << '\r' << i+1 << " / " << data.rows() << std::flush;
        io::timestamp_t ns = static_cast<io::timestamp_t>(data(i, 0) * 1e9);
        if (FLAGS_set_origin_now) ns = (ns - origin) + new_origin;

        Eigen::Vector3d xyz = wgs84.EllToXYZ(Eigen::Vector3d(data(i, 1), data(i, 2), data(i, 3)));
        io::pose_t pose(xyz(0), xyz(1), xyz(2),
                        data(i, 4), data(i, 5), data(i, 6), data(i, 7));

        trajectory.emplace_back(io::trajectory_t<io::timestamp_t>{ns, pose});
    }
    std::cout << std::endl;

    std::sort(trajectory.begin(), trajectory.end());
    std::cout << "Saving imu0_trajectory.csv ..." << std::flush;
    io::write_file(trajectory, (output_base / "imu0_trajectory.csv").string(),
                   "#timestamp [ns],tx [m],ty [m],tz [m],qw,qx,qy,qz");
    std::cout << std::endl;

    return 0;
}
