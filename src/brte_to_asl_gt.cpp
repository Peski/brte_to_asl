#define PROGRAM_NAME \
    "brte_to_asl_gt"

#define FLAGS_CASES                                                                                \

#define ARGS_CASES                                                                                 \
    ARG_CASE(input_file)                                                                           \
    ARG_CASE(output_file)

#include <algorithm>
#include <cstdint>
#include <iostream>
#include <unordered_map>
#include <utility>

// Args (Gflags extension)
#include "args.hpp"

// Boost
#include <boost/filesystem/operations.hpp>
#include <boost/range/iterator_range.hpp>

// Eigen
#include <Eigen/Core>

#include "util/macros.h"
#include "util/version.h"

#include "csv.hpp"
#include "gps.hpp"
#include "io.hpp"

namespace fs = boost::filesystem;

using imageid_t = std::size_t;

inline void ValidateFlags() { }

inline void ValidateArgs() {
    RUNTIME_ASSERT(fs::is_regular_file(ARGS_input_file));
    RUNTIME_ASSERT(!fs::exists(ARGS_output_file));
}

int main(int argc, char* argv[]) {

    // Print build info
    std::cout << PROGRAM_NAME << " (" << GetBuildInfo() << ")" << std::endl;
    std::cout << std::endl;

    // Handle help flag
    if (args::HelpRequired(argc, argv)) {
        args::ShowHelp();
        return 0;
    }

    // Parse input flags
    args::ParseCommandLineNonHelpFlags(&argc, &argv, true);

    // Check number of args
    if (argc-1 != args::NumArgs()) {
        args::ShowHelp();
        return -1;
    }

    // Parse input args
    args::ParseCommandLineArgs(argc, argv);

    // Validate input arguments
    ValidateFlags();
    ValidateArgs();

    Eigen::MatrixXd data = csv::read<double>(ARGS_input_file, ' ');
    RUNTIME_ASSERT(data.cols() == 8); // 8 columns expected
    RUNTIME_ASSERT(data.rows() > 0); // invalid file?

    GPSTransform wgs84(GPSTransform::WGS84);

    io::Trajectory trajectory;
    for (int i = 0; i < data.rows(); ++i) {
        io::timestamp_t ns = static_cast<io::timestamp_t>(data(i, 0) * 1e9);

        Eigen::Vector3d xyz = wgs84.EllToXYZ(data.block<1, 3>(i, 1).transpose());
        io::pose_t pose(xyz(0), xyz(1), xyz(2),
                        data(i, 4), data(i, 5), data(i, 6), data(i, 7));

        trajectory.emplace_back(io::trajectory_t<io::timestamp_t>{ns, pose});
    }

    std::sort(trajectory.begin(), trajectory.end());
    io::write_file(trajectory, ARGS_output_file,
                   "#timestamp [ns],tx [m], ty [m], tz [m], qw, qx, qy, qz");

    return 0;
}
