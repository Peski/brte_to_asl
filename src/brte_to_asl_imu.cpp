#define PROGRAM_NAME \
    "brte_to_asl_imu"

#define FLAGS_CASES                                                                                \
    FLAG_CASE(string, output_file, "data.csv", "Output IMU data")

#define ARGS_CASES                                                                                 \
    ARG_CASE(input_file)

#include <algorithm>
#include <cstdint>
#include <iostream>
#include <unordered_map>
#include <utility>

// Args (Gflags extension)
#include "args.hpp"

// Boost
#include <boost/filesystem/operations.hpp>
#include <boost/range/iterator_range.hpp>

// Eigen
#include <Eigen/Core>

#include "util/macros.h"
#include "util/version.h"

#include "csv.hpp"
#include "io.hpp"
#include "sequence.hpp"

namespace fs = boost::filesystem;

using imageid_t = std::size_t;

inline void ValidateFlags() { }

inline void ValidateArgs() {
    RUNTIME_ASSERT(fs::is_regular_file(ARGS_input_file));
}

int main(int argc, char* argv[]) {

    // Print build info
    std::cout << PROGRAM_NAME << " (" << GetBuildInfo() << ")" << std::endl;
    std::cout << std::endl;

    // Handle help flag
    if (args::HelpRequired(argc, argv)) {
        args::ShowHelp();
        return 0;
    }

    // Parse input flags
    args::ParseCommandLineNonHelpFlags(&argc, &argv, true);

    // Check number of args
    if (argc-1 != args::NumArgs()) {
        args::ShowHelp();
        return -1;
    }

    // Parse input args
    args::ParseCommandLineArgs(argc, argv);

    // Validate input arguments
    ValidateFlags();
    ValidateArgs();

    Eigen::MatrixXd data = csv::read<double>(ARGS_input_file, ' ');
    RUNTIME_ASSERT(data.cols() == 7); // 7 columns expected
    RUNTIME_ASSERT(data.rows() > 0); // invalid file?

    io::ImuData imu_data;
    for (int i = 0; i < data.rows(); ++i) {
        io::timestamp_t ns = static_cast<io::timestamp_t>(data(i, 0) * 1e9);
        imu_data.emplace_back(io::imu_data_t{ns,
                                             data(i, 4), data(i, 5), data(i, 6),
                                             data(i, 1), data(i, 2), data(i, 3)});
    }

    std::sort(imu_data.begin(), imu_data.end());
    io::write_file(imu_data, FLAGS_output_file,
                   "#timestamp [ns],w_x [rad s^-1],w_y [rad s^-1],w_z [rad s^-1],a_x [m s^-2],a_y [m s^-2],a_z [m s^-2]");

    return 0;
}
