#define PROGRAM_NAME \
    "brte_to_asl2"

#define FLAGS_CASES                                                                                \
    FLAG_CASE(string, output_path, "", "Output images directory")                                  \
    FLAG_CASE(string, output_file, "data.csv", "Output image timestamp association file")          \
    FLAG_CASE(bool, set_origin_now, true, "Set the time origin as the now")

#define ARGS_CASES                                                                                 \
    ARG_CASE(images_directory)                                                                     \
    ARG_CASE(groundtruth_file)

#include <algorithm>
#include <cstdint>
#include <ctime>
#include <iostream>
#include <limits>
#include <unordered_map>
#include <utility>

// Args (Gflags extension)
#include "args.hpp"

// Boost
#include <boost/filesystem/operations.hpp>
#include <boost/range/iterator_range.hpp>

// Eigen
#include <Eigen/Core>

#include "util/macros.h"
#include "util/version.h"

#include "csv.hpp"
#include "io.hpp"
#include "sequence.hpp"

namespace fs = boost::filesystem;

using imageid_t = std::size_t;

inline void ValidateArgs() {
    RUNTIME_ASSERT(fs::is_directory(ARGS_images_directory));
    RUNTIME_ASSERT(fs::is_regular_file(ARGS_groundtruth_file));
}

inline void ValidateFlags() {

    if (FLAGS_output_path.empty()) {
        FLAGS_output_path = fs::current_path().string();
    } else {
        fs::create_directories(FLAGS_output_path);
    }
}

int main(int argc, char* argv[]) {

    // Print build info
    std::cout << PROGRAM_NAME << " (" << GetBuildInfo() << ")" << std::endl;
    std::cout << std::endl;

    // Handle help flag
    if (args::HelpRequired(argc, argv)) {
        args::ShowHelp();
        return 0;
    }

    // Parse input flags
    args::ParseCommandLineNonHelpFlags(&argc, &argv, true);

    // Check number of args
    if (argc-1 != args::NumArgs()) {
        args::ShowHelp();
        return -1;
    }

    // Parse input args
    args::ParseCommandLineArgs(argc, argv);

    // Validate input arguments
    ValidateArgs();
    ValidateFlags();

    io::timestamp_t new_origin = 0;
    if (FLAGS_set_origin_now)
        new_origin = static_cast<double>(std::time(nullptr)) * 1e9;

    Eigen::MatrixXd data = csv::read<double>(ARGS_groundtruth_file, ' ');
    RUNTIME_ASSERT(data.cols() == 9); // 9 columns expected
    RUNTIME_ASSERT(data.rows() > 0); // invalid file?

    io::timestamp_t origin = std::numeric_limits<io::timestamp_t>::max();
    std::unordered_map<imageid_t, io::timestamp_t> id_to_timestamp;
    for (int i = 0; i < data.rows(); ++i) {
        imageid_t id = static_cast<imageid_t>(data(i, 0));
        io::timestamp_t ns = static_cast<io::timestamp_t>(data(i, 1) * 1e9);
        RUNTIME_ASSERT(id_to_timestamp.find(id) == id_to_timestamp.end()); // id expected to be an unique identifier
        id_to_timestamp[id] = ns;
        if (ns < origin) origin = ns;
    } 

    io::Records records;
    for (const fs::directory_entry& entry : boost::make_iterator_range(fs::directory_iterator(ARGS_images_directory), {})) {
        std::string filename = entry.path().filename().string();
        imageid_t id = static_cast<imageid_t>(sequence_id::get_sid(filename));
        RUNTIME_ASSERT(id_to_timestamp.find(id) != id_to_timestamp.end()); // id expected to be found in groundtruth file

        io::timestamp_t ns = id_to_timestamp.at(id) - origin + new_origin;

        std::string new_filename = io::to_string(ns, 10) + fs::extension(filename);
        fs::path new_path = fs::path(FLAGS_output_path);
        new_path /= new_filename;

        fs::copy_file(entry.path(), new_path);

        records.emplace_back(std::make_pair(ns, new_filename));
    }

    std::sort(records.begin(), records.end());
    io::write_file(records,
                   (fs::path(ARGS_images_directory).parent_path() / FLAGS_output_file).string(),
                   "#timestamp [ns],filename");

    return 0;
}

