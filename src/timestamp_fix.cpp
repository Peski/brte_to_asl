#define PROGRAM_NAME \
    "timestamp_fix"

#define FLAGS_CASES                                                                                \

#define ARGS_CASES                                                                                 \
    ARG_CASE(input_file)                                                                           \
    ARG_CASE(output_file)

#include <algorithm>
#include <iostream>
#include <vector>

// Args (Gflags extension)
#include "args.hpp"

// Boost
#include <boost/filesystem/operations.hpp>
#include <boost/range/iterator_range.hpp>

// Eigen
#include <Eigen/Core>

#include "util/macros.h"
#include "util/version.h"

#include "csv.hpp"
#include "io.hpp"

namespace fs = boost::filesystem;

using imageid_t = std::size_t;

inline void ValidateFlags() { }

inline void ValidateArgs() {
    RUNTIME_ASSERT(fs::is_regular_file(ARGS_input_file));
    RUNTIME_ASSERT(!fs::exists(ARGS_output_file));
}

int main(int argc, char* argv[]) {

    // Print build info
    std::cout << PROGRAM_NAME << " (" << GetBuildInfo() << ")" << std::endl;
    std::cout << std::endl;

    // Handle help flag
    if (args::HelpRequired(argc, argv)) {
        args::ShowHelp();
        return 0;
    }

    // Parse input flags
    args::ParseCommandLineNonHelpFlags(&argc, &argv, true);

    // Check number of args
    if (argc-1 != args::NumArgs()) {
        args::ShowHelp();
        return -1;
    }

    // Parse input args
    args::ParseCommandLineArgs(argc, argv);

    // Validate input arguments
    ValidateFlags();
    ValidateArgs();

    Eigen::MatrixXd data = csv::read<double>(ARGS_input_file, ',');
    RUNTIME_ASSERT(data.cols() == 7); // 7 columns expected
    RUNTIME_ASSERT(data.rows() > 0); // invalid file?

    io::ImuData output_data;
    for (int idx = 0; idx < data.rows(); ++idx) {
        const Eigen::Matrix<double, 1, 7>& row_data = data.row(idx);
        output_data.emplace_back(io::imu_data_t(row_data(0), row_data(1), row_data(2), row_data(3), row_data(4), row_data(5), row_data(6)));
    }

    std::sort(output_data.begin(), output_data.end());
    io::write_file(output_data, ARGS_output_file,
                   "#timestamp [ns],w_x [rad s^-1],w_y [rad s^-1],w_z [rad s^-1],a_x [m s^-2],a_y [m s^-2],a_z [m s^-2]");

    return 0;
}
